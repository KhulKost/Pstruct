import queue
import unittest

class TestStackMethod(unittest.TestCase):
	def test_MethodStack(self):
		myqueue = queue.Queue()
		count = 100
		while count != 0:
			myqueue.enqueue(count)
			count = count - 1
		count = 1
		while count != 100:
			item = myqueue.dequeue()
			self.assertEqual(item, count)
			myqueue.pop()
			count = count + 1