import LinkedList
class Set():

	def __init__(self, size, type):
		self.size = size
		self.type = type
		self.storage = LinkedList.LinkedList()

	def add(self, item):
		listitem = self.storage.search(item)
		len = self.storage.len()
		if self.type != type(item):
			return False
		if len == self.size:
			return False
		if listitem == None:
			self.storage.append(item)
			return True
		else:
			return False

	def contains(self, item):
		setitem = self.storage.search(item)
		if setitem == item:
			return True
		else:
			return False

	def remove(self, item):
		listitem = self.storage.search(item)
		if listitem is not None:
			self.storage.delete(item)
			return True
		return False

	def count(self):
		len = self.storage.len()
		return len

	def union(self,myset):
		mysetlen = myset.count()
		while mysetlen != 0:
			result = self.add(myset.storage.item)
			if result == False or result == True:
				myset.storage = myset.storage.next
				mysetlen -=1
		return self

	def intersection(self, set1):
		sizeset = self.count() + set1.count()
		typeset = type(set1.storage.item) 
		tmp = Set(sizeset, typeset)
		longset = 0
		shortset = 0
		setlen = set1.storage.len()
		selflen = self.storage.len()
		if setlen > selflen:
			longset = set1
			shortset = self
		elif setlen < selflen:
			longset = self
			shortset = set1
		elif setlen == selflen:
			longset = set1
			shortset = self
		shortlen = shortset.count()
		while shortlen != 0:
			if longset.contains(shortset.storage.item):
				tmp.add(shortset.storage.item)
				shortset.storage = shortset.storage.next
				shortlen -= 1
			else:
				shortset.storage = shortset.storage.next
				shortlen -= 1
		return tmp



set1 = Set(10, int)
set2 = Set(10, int)
bol = set1.add(1)
bol = set1.add(2)
bol = set1.add(3)
bol = set1.add(4)
bol = set1.add(5)
set2.add(1)
set2.add(3)
set2.add(5)
#set3 = set1.union(set2)
#sea = set1.contains(1)
set4 = set1.intersection(set2)
print(bol, set4.storage.item)