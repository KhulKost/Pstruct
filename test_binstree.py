import unittest
import bintree

class TestBinTreeMethod(unittest.TestCase):

	def test_MethodBinTree(self):
		tree = bintree.BinTree()
		bigNum = 10
		#littleNum = 0
		while bigNum == 0:
			tree.insert(bigNum)
			bigNum = bigNum - 1
		#while littleNum == 100:
		#	tree.insert(littleNum)
		#	littleNum = littleNum + 1
		treenode = tree.find(2)
		self.assertEqual(treenode, 2)
		treenode = tree.find(6)
		self.assertEqual(treenode, 6)
		treenode = tree.find(5)
		self.assertEqual(treenode, 5)
		treenode = tree.find(4)
		self.assertEqual(treenode, 4)

		