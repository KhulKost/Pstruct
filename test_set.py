import unittest
import set

class TestSetMethod(unittest.TestCase):

	def test_MethodSetadd(self):
		myset = set.Set(100, int)
		number = 0
		while number != 100:
			myset.add(number)
			self.assertEqual(myset.storage.item, number)
			number +=1

	def test_MethodSetremove(self):
		myset = set.Set(100, int)
		number = 0
		count = 0
		while number != 100:
			myset.add(number)
			number +=1
		while count != 100:
			myset.remove(count)
			count +=1

