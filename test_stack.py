import stack
import unittest

class TestStackMethod(unittest.TestCase):
	def test_MethodStack(self):
		mystack = stack.Stack()
		count = 100
		while count != 0:
			mystack.push(count)
			count = count - 1
		count = 1
		while count != 100:	
			self.assertEqual(mystack.peek(), count)
			mystack.pop()
			count = count + 1