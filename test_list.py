import unittest
import LinkedList

class TestListMethod(unittest.TestCase):

	def test_MethodList(self):
		list = LinkedList.LinkedList()
		count = 3
		while count != 0:
			list.prepend(count)
			self.assertEqual(list.item, 3)
			count = count - 1
			list.prepend(count)
			self.assertEqual(list.item, 2)
			count = count - 1
			list.prepend(count)
			self.assertEqual(list.item, 1)
			count = count - 1
			list.prepend(count)
			self.assertEqual(list.item, 0)

		len = list.len()
		self.assertEqual(len, 4)
		list.delete(2)
		self.assertEqual(list.next.next.item, 3)
		len = list.len()
		self.assertEqual(len, 3)
		element = list.search(3)
		self.assertEqual(element, 3)
		self.assertTrue(list.check_empty)
		list.append(4)
		self.assertEqual(list.next.next.next.item, 4)
		len = list.len()
		self.assertEqual(len, 4)
		list2 = LinkedList.LinkedList()
		count = 5
		while count != 0:
			list2.prepend(count)
			count = count -1

		newList = list.сombine(list2)
		len = newList.len()
		print(list.len())
		self.assertEqual(len, 9)






