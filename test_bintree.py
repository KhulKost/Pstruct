import unittest
import bintree

class TestBinTreeMethod(unittest.TestCase):

	def test_MethodBinTree(self):
		tree = bintree.BinTree()
		num = 0
		while num != 10:
			tree.insert(num)
			num = num + 1
		treenode,x = tree.find(9)
		self.assertEqual(treenode.node, 9)
		treenode,x = tree.find(6)
		self.assertEqual(treenode.node, 6)
		treenode,x = tree.find(5)
		self.assertEqual(treenode.node, 5)
		treenode,x = tree.find(4)
		self.assertEqual(treenode.node, 4)
		tree.remove(2)
		self.assertEqual(tree.right.right.node, 3)

