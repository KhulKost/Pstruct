
class BinTree():

	def __init__(self):
		self.node = None
		self.left = None
		self.right = None

	def insert(self, item):
		tree = BinTree()
		while True:
			if self.node is None:
				self.node = item
				break
			if item < self.node:
				if self.left is None:
					tree.node = item
					self.left = tree
					break
				elif self.left != None:
					self = self.left
			elif item > self.node:
				if self.right is None:
					tree.node = item
					self.right = tree
					break
				elif self.right != None:
					self = self.right

	def find(self, item, parent=None):
			if item < self.node:
				if self.left is None:
					return None, None
				return self.left.find(item, self)
			elif item > self.node:
				if self.right is None:
					return None, None
				return self.right.find(item, self)
			else:
				return self, parent

	def children_count(self):
		cht = 0
		if self.left != None:
			cht = 1
		if self.right != None:
			cht +=1
		return cht

	def remove(self, item):
		newtree, parent = self.find(item)
		if newtree is not None:
			children_count = newtree.children_count()
		if children_count == 0:
			if parent != None:
				if parent.left is newtree:
					parent.left = None
				else:
					parent.right = None
					print(newtree)
				del newtree
			else:
				self.node = None
		elif children_count == 1:
			if newtree.left != None:
				n = newtree.left
			else:
				n = newtree.right
			if parent != None:
				if parent.left is newtree:
					parent.left = n
				else:
					parent.right = n
				del newtree
			else:
				self.left = n.left
				self.right = n.right
				self.node = n.node
		else:
			parent = newtree
			succ = newtree.right
			while succ.left:
				parent = succ
				succ = succ.left
			newtree.node = succ.node
			if parent.left == succ:
				parent.left = succ.right
			else:
				parent.right = succ.right



#b = BinTree()
#b.insert(1)
#b.insert(66)
#b.insert(67)
#b.insert(65)
#b.insert(55)
#b.insert(33)
#b.remove(67)
#print(b.right.right)